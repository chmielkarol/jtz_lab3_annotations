package lab03;

import java.awt.EventQueue;
import java.awt.Image;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;
import java.awt.BorderLayout;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;
import javax.swing.UIManager;
import java.awt.Color;
import javax.swing.JButton;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.awt.event.ActionEvent;
import javax.swing.border.SoftBevelBorder;
import javax.swing.border.BevelBorder;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.JTextField;

public class Window implements WindowInterface{

	private JFrame frmKkoKrzyyk;
	private Board board;
	private int board_n;
	
	private JButton buttons[][];
	private JButton btnNewButton_1;
	private List<Method> levels;
	private DefaultListModel<String> model_1;
	private JList<String> list_1;
	private List<String> cl_names;
	private JPanel panel_3;
	private JTabbedPane tabbedPane;
	private JButton btnNewButton;
	
	private ImageIcon imageX;
	private ImageIcon imageO;
	private JTextField textField;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Window window = new Window();
					window.frmKkoKrzyyk.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Window() {
		int n;
		do {
			n = Integer.parseInt(JOptionPane.showInputDialog(null, "Podaj rozmiar planszy (n x n; n>=5): "));
		} while(n < 5);
		board_n = n;
		
		initialize();
		
		newGame();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmKkoKrzyyk = new JFrame();
		frmKkoKrzyyk.setResizable(false);
		frmKkoKrzyyk.setTitle("K\u00F3\u0142ko - krzy\u017Cyk");
		frmKkoKrzyyk.setBounds(100, 100, 671, 620);
		frmKkoKrzyyk.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		frmKkoKrzyyk.getContentPane().add(tabbedPane, BorderLayout.CENTER);
		
		JPanel panel = new JPanel();
		tabbedPane.addTab("Gra", null, panel, null);
		panel.setLayout(null);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Plansza", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel_2.setBounds(120, 17, 512, 523);
		panel.add(panel_2);
		panel_2.setLayout(null);
		
		panel_3 = new JPanel();
		panel_3.setBounds(6, 16, 500, 500);
		panel_2.add(panel_3);
		panel_3.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null));
		panel_3.setLayout(null);
		
		btnNewButton = new JButton("Restart gry");
		btnNewButton.setEnabled(false);
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				newGame();
			}
		});
		btnNewButton.setBounds(10, 115, 103, 64);
		panel.add(btnNewButton);
		
		JPanel panel_6 = new JPanel();
		panel_6.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Liczba ruch\u00F3w", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel_6.setBounds(12, 229, 98, 43);
		panel.add(panel_6);
		panel_6.setLayout(null);
		
		textField = new JTextField();
		textField.setBounds(6, 16, 86, 20);
		panel_6.add(textField);
		textField.setEditable(false);
		textField.setColumns(8);
		
		Image imgX = new ImageIcon(Window.class.getResource("/lab03/imageX.png")).getImage();
		Image newimg = imgX.getScaledInstance(500/board_n, 500/board_n, java.awt.Image.SCALE_SMOOTH);
		imageX = new ImageIcon(newimg);
		Image imgO = new ImageIcon(Window.class.getResource("/lab03/imageO.png")).getImage();
		Image newimg2 = imgO.getScaledInstance(500/board_n, 500/board_n, java.awt.Image.SCALE_SMOOTH);
		imageO = new ImageIcon(newimg2);

		JPanel panel_1 = new JPanel();
		tabbedPane.addTab("Ustawienia", null, panel_1, null);
		panel_1.setLayout(null);
		
		JPanel panel_4 = new JPanel();
		panel_4.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Wyb\u00F3r strategii", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel_4.setBounds(108, 57, 188, 307);
		panel_1.add(panel_4);
		panel_4.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(6, 16, 176, 240);
		panel_4.add(scrollPane);
		
		DefaultListModel<String> model = new DefaultListModel<>();
		JList<String> list = new JList<>();
		list.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent arg0) {
				if(list.getSelectedIndex()!=-1) btnNewButton_1.setEnabled(true);
				else btnNewButton_1.setEnabled(false);
			}
		});
		list.setModel(model);
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrollPane.setViewportView(list);
		
		//UPDATE STRATEGIES LIST
		File f = new File("D:\\Users\\Karol\\Documents\\Uczelnia\\Semestr VI\\JAVA\\lab\\lab03\\bin\\strategies");
		File[] files = f.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return name.endsWith("class");
			}
		});
		cl_names = new ArrayList<>();
		for(File file : files) {
			String name = file.getName();
			int len = name.length();
			name = name.substring(0, len - 6); //len(".class") = 6
			try {
				URLClassLoader cl = new URLClassLoader(new URL[] {new URL("file:/D:\\Users\\Karol\\Documents\\Uczelnia\\Semestr VI\\JAVA\\lab\\lab03\\bin\\strategies/")});
				Class<?> c = cl.loadClass(name);
				
				Annotation[] annotations = c.getAnnotations();
				
				for(Annotation annotation : annotations) {
					if(annotation instanceof StrategyAnnotation) {
						String strategy_name = ((StrategyAnnotation) annotation).name();
						model.addElement(strategy_name);
						cl_names.add(name);
					}
				}
				cl.close();
			} catch (IOException | ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		btnNewButton_1 = new JButton("U\u017Cyj");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int index = list.getSelectedIndex();
				String cl_name = cl_names.get(index);
				Class<?> c = null;
				try {
					URLClassLoader cl = new URLClassLoader(new URL[] {new URL("file:/D:\\Users\\Karol\\Documents\\Uczelnia\\Semestr VI\\JAVA\\lab\\lab03\\bin\\strategies/")});
					c = cl.loadClass(cl_name);
					cl.close();
				} catch (ClassNotFoundException | IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				Method[] methods = c.getMethods();
				model_1.clear();
				levels = new ArrayList<>();
				for(Method method : methods) {
					Annotation[] annotations = method.getAnnotations();
					for(Annotation annotation : annotations) {
						if(annotation instanceof LevelAnnotation) {
							String annotation_name = ((LevelAnnotation) annotation).name();
							model_1.addElement(annotation_name);
							levels.add(method);
						}
					}
				}
				try {
					board.setObj(c.newInstance());
				} catch (InstantiationException | IllegalAccessException e) {
					e.printStackTrace();
				}
				System.gc();
			}
		});
		btnNewButton_1.setEnabled(false);
		btnNewButton_1.setBounds(51, 277, 89, 23);
		panel_4.add(btnNewButton_1);
		
		JPanel panel_5 = new JPanel();
		panel_5.setLayout(null);
		panel_5.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Wyb\u00F3r trybu", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel_5.setBounds(377, 58, 188, 307);
		panel_1.add(panel_5);
		
		JButton btnUstaw = new JButton("Ustaw");
		btnUstaw.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int index = list_1.getSelectedIndex();
				Method method = levels.get(index);
				board.setMethod(method);
				tabbedPane.setSelectedIndex(0);
			}
		});
		btnUstaw.setEnabled(false);
		btnUstaw.setBounds(51, 277, 89, 23);
		panel_5.add(btnUstaw);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(6, 16, 176, 240);
		panel_5.add(scrollPane_1);

		model_1 = new DefaultListModel<>();
		list_1 = new JList<>();
		list_1.setModel(model_1);
		list_1.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list_1.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent arg0) {
				if(list_1.getSelectedIndex()!=-1) btnUstaw.setEnabled(true);
				else btnNewButton_1.setEnabled(false);
			}
		});
		scrollPane_1.setViewportView(list_1);
		
	}

	@Override
	public void userWon() {
		JOptionPane.showMessageDialog(null, "Wygra�e�!");
		System.exit(0);
	}

	@Override
	public void pcWon() {
		JOptionPane.showMessageDialog(null, "Przegra�e�!");
		System.exit(0);
	}
	
	@Override
	public void remis() {
		JOptionPane.showMessageDialog(null, "Remis");
		System.exit(0);
	}

	public void repaint() {
		for(int i = 0; i < board_n; ++i) {
			for(int j = 0; j < board_n; ++j) {
				if(board.tab[i][j] == 'X') {
					buttons[i][j].setIcon(imageX);
					buttons[i][j].setEnabled(false);
				}
				else if(board.tab[i][j] == 'O') {
					buttons[i][j].setIcon(imageO);
					buttons[i][j].setEnabled(false);
				}
			}
		}
		textField.setText(Integer.toString(board.movements));
	}
	
	private void newGame() {
		board = new Board(board_n);
		board.setGUI(this);
		
		panel_3.removeAll();
		buttons = new JButton[board_n][board_n];
		
		for(int i = 0; i < board_n; ++i) {
			for(int j = 0; j < board_n; ++j) {
				int temp_i = i;
				int temp_j = j;
				JButton button = new JButton();
				button.setBounds(i*500/board_n, j*500/board_n, 500/board_n, 500/board_n);
				button.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						button.setIcon(imageX);
						button.setEnabled(false);
						
						board = board.move(temp_i, temp_j);
						repaint();
					}
				});
				buttons[i][j] = button;
				panel_3.add(button);
			}
		}
		
		btnNewButton.setEnabled(true);
		tabbedPane.setSelectedIndex(1);
		JOptionPane.showMessageDialog(null, "Wybierz strategie i jej tryb.");
	}
}
