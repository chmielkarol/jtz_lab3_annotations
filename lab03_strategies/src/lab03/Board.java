package lab03;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Scanner;

public class Board {

	public char[][] tab;
	public int n;
	public int movements;
	
	private Method method;
	private Object obj;
	private WindowInterface wi;
	
	Board(int n){
		if(n < 5) n = 5;
		this.n = n;
		movements = 0;
		
		//Inicjalizacja planszy
		tab = new char[n][n];
		for(int i = 0; i < n; ++i)
			for(int j = 0; j < n; ++j)
				tab[i][j] = '-';
	}
	
	public Board(Board b){
		n = b.n;
		method = b.method;
		obj = b.obj;
		wi = b.wi;
		movements = b.movements;
		tab = new char[n][n];
		for(int i = 0; i < n; ++i)
			for(int j = 0; j < n; ++j)
				tab[i][j] = b.tab[i][j];
	}
	
	public Board move(int x, int y) {
		++movements;
		tab[x][y] = 'X';
		if(win() == 'X') wi.userWon();
		if(remis()) wi.remis();
		Board newboard = null;
		try {
			newboard = (Board)method.invoke(obj, this);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			e.printStackTrace();
		}
		if(newboard.win() == 'O') wi.pcWon();
		return newboard;
	}
	
	public void setMethod(Method m) {
		method = m;
	}
	
	public void setObj(Object o) {
		obj = o;
	}

	public void setGUI(WindowInterface wi) {
		this.wi = wi;
	}
	
	public boolean remis() {
		for(int i = 0; i < n; ++i)
			for(int j = 0; j < n; ++j)
				if(tab[i][j] == '-') return false;
		return true;
	}
	
	public char win() {
		//X - human won, O - PC won, '-' - no win
		
		int count = 0;
		char chr = '-';
		
		for(int i = 0; i < n; ++i){
			count = 0;
			chr = '-';
			
			for(int j = 0; j < n; ++j) {
				if(tab[i][j] == 'X') {
					if(chr == 'X') {
						++count;
						if(count == 5) return chr;
					}
					else {
						count = 1;
						chr = 'X';
					}
				}
				else {
					if(tab[i][j] == 'O') {
						if(chr == 'O') {
							++count;
							if(count == 5) return chr;
						}
						else {
							count = 1;
							chr = 'O';
						}
					}
					else chr = '-';
				}
			}
		}
		
		for(int i = 0; i < n; ++i){
			count = 0;
			chr = '-';
			
			for(int j = 0; j < n; ++j) {
				if(tab[j][i] == 'X') {
					if(chr == 'X') {
						++count;
						if(count == 5) return chr;
					}
					else {
						count = 1;
						chr = 'X';
					}
				}
				else {
					if(tab[j][i] == 'O') {
						if(chr == 'O') {
							++count;
							if(count == 5) return chr;
						}
						else {
							count = 1;
							chr = 'O';
						}
					}
					else chr = '-';
				}
			}
		}
		
		for(int i = 0; i < n; ++i) {
			int x = i;
			int y = 0;
			count = 0;
			chr = '-';
			
			while((x < n) && (y < n)) {
				if(tab[x][y] == 'X') {
					if(chr == 'X') {
						++count;
						if(count == 5) return chr;
					}
					else {
						count = 1;
						chr = 'X';
					}
				}
				else {
					if(tab[x][y] == 'O') {
						if(chr == 'O') {
							++count;
							if(count == 5) return chr;
						}
						else {
							count = 1;
							chr = 'O';
						}
					}
					else chr = '-';
				}
				++x;
				++y;
			}
		}
		
		for(int i = 0; i < n; ++i) {
			int x = 0;
			int y = i;
			count = 0;
			chr = '-';
			
			while((x < n) && (y < n)) {
				if(tab[x][y] == 'X') {
					if(chr == 'X') {
						++count;
						if(count == 5) return chr;
					}
					else {
						count = 1;
						chr = 'X';
					}
				}
				else {
					if(tab[x][y] == 'O') {
						if(chr == 'O') {
							++count;
							if(count == 5) return chr;
						}
						else {
							count = 1;
							chr = 'O';
						}
					}
					else chr = '-';
				}
				++x;
				++y;
			}
		}
		
		for(int i = 0; i < n; ++i) {
			int x = i;
			int y = 0;
			count = 0;
			chr = '-';
			
			while((x < n) && (y >= 0)) {
				if(tab[x][y] == 'X') {
					if(chr == 'X') {
						++count;
						if(count == 5) return chr;
					}
					else {
						count = 1;
						chr = 'X';
					}
				}
				else {
					if(tab[x][y] == 'O') {
						if(chr == 'O') {
							++count;
							if(count == 5) return chr;
						}
						else {
							count = 1;
							chr = 'O';
						}
					}
					else chr = '-';
				}
				++x;
				--y;
			}
		}
		
		for(int i = 0; i < n; ++i) {
			int x = 0;
			int y = i;
			count = 0;
			chr = '-';
			
			while((x < n) && (y >= 0)) {
				if(tab[x][y] == 'X') {
					if(chr == 'X') {
						++count;
						if(count == 5) return chr;
					}
					else {
						count = 1;
						chr = 'X';
					}
				}
				else {
					if(tab[x][y] == 'O') {
						if(chr == 'O') {
							++count;
							if(count == 5) return chr;
						}
						else {
							count = 1;
							chr = 'O';
						}
					}
					else chr = '-';
				}
				++x;
				--y;
			}
		}
		
		return '-';
	}
}
