import lab03.Board;
import lab03.LevelAnnotation;
import lab03.StrategyAnnotation;

@StrategyAnnotation(name = "Strategia przeskadzania graczowi")
public class Strategy4 {
	
	@LevelAnnotation(name = "Poziom 0")
	public Board level0(Board in) {
		float lowest_dist = Float.MAX_VALUE;
		int lowest_dist_x = 0;
		int lowest_dist_y = 0;
		
		for(int i = 0; i < in.n; ++i) {
			for(int j = 0; j < in.n; ++j) {
				if(in.tab[i][j] == '-') {
					//movement can be done

					float dist = 0;
					
					for(int a = 0; a < in.n; ++a) {
						for(int b = 0; b < in.n; ++b) {
							if(in.tab[a][b] == 'X') dist += Math.sqrt((i-a)*(i-a) + (j-b)*(j-b));
						}
					}
					
					if(dist < lowest_dist) {
						lowest_dist = dist;
						lowest_dist_x = i;
						lowest_dist_y = j;
					}
				}
			}
		}
		
		Board newBoard = new Board(in);
		newBoard.tab[lowest_dist_x][lowest_dist_y] = 'O';
		return newBoard;
	}
}