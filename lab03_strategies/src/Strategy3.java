import java.util.Random;

import lab03.Board;
import lab03.LevelAnnotation;
import lab03.StrategyAnnotation;

@StrategyAnnotation(name = "Strategia symetrii")
public class Strategy3 {
	
	@LevelAnnotation(name = "Poziom 0")
	public Board level0(Board in) {
		for(int i = 0; i < in.n; ++i) {
			for(int j = 0; j < in.n; ++j) {
				if((in.tab[i][j] == 'X') && (in.tab[in.n-i-1][j] == '-')) {
					Board newBoard = new Board(in);
					newBoard.tab[in.n-i-1][j] = 'O';
					return newBoard;
				}
			}
		}

		Random gen = new Random();
		int x, y;
		do {
			x = gen.nextInt(in.n);
			y = gen.nextInt(in.n);
		} while(in.tab[x][y] != '-');
		Board newBoard = new Board(in);
		newBoard.tab[x][y] = 'O';
		return newBoard;
	}
	
	@LevelAnnotation(name = "Poziom 1")
	public Board level1(Board in) {
		for(int i = 0; i < in.n; ++i) {
			for(int j = 0; j < in.n; ++j) {
				if((in.tab[i][j] == 'X') && (in.tab[in.n-i-1][in.n-j-1] == '-')) {
					Board newBoard = new Board(in);
					newBoard.tab[in.n-i-1][in.n-j-1] = 'O';
					return newBoard;
				}
			}
		}

		Random gen = new Random();
		int x, y;
		do {
			x = gen.nextInt(in.n);
			y = gen.nextInt(in.n);
		} while(in.tab[x][y] != '-');
		Board newBoard = new Board(in);
		newBoard.tab[x][y] = 'O';
		return newBoard;
	}
}