import java.util.Random;

import lab03.Board;
import lab03.LevelAnnotation;
import lab03.StrategyAnnotation;

@StrategyAnnotation(name = "Strategia losowa")
public class Strategy {
	private int x_last = 2;
	private int y_last = 2;
	
	@LevelAnnotation(name = "Poziom 0")
	public Board level0(Board in) {
		Random gen = new Random();
		int x, y;
		do {
			x = gen.nextInt(in.n);
			y = gen.nextInt(in.n);
		} while(in.tab[x][y] != '-');
		Board newBoard = new Board(in);
		newBoard.tab[x][y] = 'O';
		return newBoard;
	}

	@LevelAnnotation(name = "Poziom 1")
	public Board level1(Board in) {
		Random gen = new Random();
		int x, y;
		do {
			int dx = (int) Math.round(gen.nextGaussian());
			int dy = (int) Math.round(gen.nextGaussian());
			x = x_last + dx;
			y = y_last + dy;
			if ((x < 0) || (x >= in.n) || (y < 0) || (y >= in.n)) {
				x = x_last;
				y = y_last;
			}
		} while(in.tab[x][y] != '-');
		Board newBoard = new Board(in);
		newBoard.tab[x][y] = 'O';
		x_last = x;
		y_last = y;
		return newBoard;
	}
}
