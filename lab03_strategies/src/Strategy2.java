import lab03.Board;
import lab03.LevelAnnotation;
import lab03.StrategyAnnotation;

@StrategyAnnotation(name = "Strategia unikania gracza")
public class Strategy2 {
	
	@LevelAnnotation(name = "Poziom 0")
	public Board level0(Board in) {
		float greatest_dist = -1;
		int greatest_dist_x = 0;
		int greatest_dist_y = 0;
		
		for(int i = 0; i < in.n; ++i) {
			for(int j = 0; j < in.n; ++j) {
				if(in.tab[i][j] == '-') {
					//movement can be done

					float dist = 0;
					
					for(int a = 0; a < in.n; ++a) {
						for(int b = 0; b < in.n; ++b) {
							if(in.tab[a][b] == 'X') dist += Math.sqrt((i-a)*(i-a) + (j-b)*(j-b));
						}
					}
					
					if(dist > greatest_dist) {
						greatest_dist = dist;
						greatest_dist_x = i;
						greatest_dist_y = j;
					}
				}
			}
		}
		
		Board newBoard = new Board(in);
		newBoard.tab[greatest_dist_x][greatest_dist_y] = 'O';
		return newBoard;
	}
}